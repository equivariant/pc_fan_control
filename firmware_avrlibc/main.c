#define F_CPU 16000000ul

#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>
#include <stdlib.h>
#include <util/atomic.h>
#include <util/delay.h> 

/* ADC pin the potentiometer is connected to. */
const uint8_t ADC_CHANNEL = 0;

/*
 * Number of pulses from the fan's tachometer. This counter is reset to 0 every
 * second by the TIMER0 interrupt. The tachometer emits a pulse twice per
 * revolution, so the max speed we can support without uint8_t wraparound is
 * 255*60/2 = 7650 RPM. The fan I'm using is rated for 1700 RPM.
 */
volatile uint8_t pulses = 0;

/* Number of times TIMER0 interrupt has triggered. */
volatile uint8_t ticks = 0;

/* Current RPM. */
volatile uint16_t rpm = 0;

/* Signals that a new RPM has been calculated. */
volatile uint8_t rpm_ready = 0;

/* Count pulses from fan's tachometer. */
ISR (INT0_vect) {
    pulses += 1;
}

/* Interrupt fired at 16MHz/1024/125 = 125Hz. */
ISR (TIMER0_COMPA_vect) {
    uint8_t tmp_pulses;

    /*
     * Wraps around every 125 time the interrupt fires. The interrupt fires at
     * 125 Hz, so this wraps around once every second.
     */
    ticks = (ticks + 1) % 125;
    if (ticks == 0) {
        /*
         * INT0 runs at higher priority than TIMER0_COMPA, so need to do this
         * atomically in case INT0 interrupt fires.
         */
        ATOMIC_BLOCK(ATOMIC_FORCEON) {
            tmp_pulses = pulses;
            pulses = 0;
        }

        /*
         * Tachometer emits a pulse twice per revolution. This is calculated
         * once per second.
         */
        rpm = (uint16_t)tmp_pulses * 60 / 2;
        rpm_ready = 1;
    }
}

int uart_putchar(char c) {
    /* Wait until USART0 is ready to accept data for transmission. */
    loop_until_bit_is_set(UCSR0A, UDRE0);

    UDR0 = c;
}

int uart_putstr(char *s) {
    while (*s) {
        uart_putchar(*s++);
    }
}

int main(void) {
    uint16_t pot;
    uint16_t tmp_rpm;
    uint8_t tmp_rpm_ready;

    /*
     * Max allowable RPM is 7650, so 4 decimal digits plus 1 null terminator
     * are needed.
     */ 
    char rpm_str[5];

    /*
     * Initialize ADC.
     *
     * Enable ADC, set prescaler to F_CPU/1024, and set ADC reference voltags
     * to AVCC (connected to VCC on Arduino Diecimila board).
     */
    ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
    ADMUX = _BV(REFS0) | (ADC_CHANNEL & 0x07);

    /*
     * Initialize PWM.
     *
     * Enable OC1A pin (Arduino pin 9), set prescaler to /1, and use phase
     * correct PWM mode with upper value of 320. This results in a PWM
     * frequency of F_CPU/(2*PRESCALER*ICR1) = 16MHz/(2*1*320) = 25kHz (see
     * section 15.9.4 of datasheet), which is the recommended frequency in
     * Intel's "4-Wire Pulse Width Modulation (PWM) Controlled Fans - Revision
     * 1.3" document.
     */
    TCCR1A = _BV(COM1A1) | _BV(WGM11);
    TCCR1B = _BV(WGM13) | _BV(CS10);
    ICR1 = 320;
    DDRB = _BV(DDB1);

    /*
     * Initialize timer.
     *
     * Set to "clear timer on compare match" mode, which clears the timer when
     * it reaches the value in OCR0A. Prescaler is set to /1024. Enable
     * interrupt when we reach the value in OCR0A.
     *
     * This results in the interrupt being fired at a rate of
     * F_CPU/(PRESCALER*(OCR0A+1)) = 16MHz/(1024*125) = 125Hz.
     */
    TCCR0A = _BV(WGM01);
    TCCR0B = _BV(CS02) | _BV(CS00);
    OCR0A = 125 - 1;
    TIMSK0 = _BV(OCIE0A);

    /*
     * Initialize external interrupt on rising edge of INT0 pin (Arduino pin 2).
     */
    EICRA = _BV(ISC01) | _BV(ISC00);
    EIMSK = _BV(INT0);

    /*
     * Initialize UART.
     *
     * Enable asynchronous double speed mode and enable transmit pin. Configure
     * baud rate to 9600 per Table 19-1 of datasheet.
     */
    UCSR0A = _BV(U2X0);
    UCSR0B = _BV(TXEN0);
    UBRR0 = F_CPU / (8 * 9600ul) - 1;

    /* Enable all configured interrupts. */
    sei();

    while (1) {
        /* Read ADC. */
        ADCSRA |= _BV(ADSC);
        while (ADCSRA & _BV(ADSC));
        pot = ADC;

        /*
         * Write PWM.
         *
         * ADC reading is in range 0-1023, which is converted to range 0-255
         * for PWM.
         */
        OCR1A = (pot + 1) * 5 / 16;

        /* Write status to UART. */
        if (rpm_ready) {
            /*
             * Atomic compare-and-set not needed here because we read the `rpm`
             * variable after clearing `rpm_ready`.
             */
            rpm_ready = 0;

            ATOMIC_BLOCK(ATOMIC_FORCEON) {
                /* Access 16-bit variable atomically. */
                tmp_rpm = rpm;
            }
            itoa(tmp_rpm, rpm_str, 10);
            uart_putstr(rpm_str);
            uart_putchar('\r');
            uart_putchar('\n');
        }
    }

    return 0;
}
