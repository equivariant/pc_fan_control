# 12V PC fan controller

ATmega168-based controller for 12V 4-pin PC fan. Speed can be adjusted with a potentiometer and current RPM is output to serial port.


## Hardware

Bill of materials:

* ATmega168/328-based Arduino (I used an Arduino Diecimila)
* AVR programmer (I used a Bus Pirate)
* Breadboard
* Solid-core 22 AWG wire
* 10kΩ linear potentiometer
* Assorted resistors

Construct the circuit shown in [hardware/pc_fan_control.pdf](hardware/pc_fan_control.pdf) on a breadboard.

Fan pinout is:

1. GND
2. +12V
3. Tachometer
4. PWM

The GND pin is usually marked with a black wire and/or a "1" or arrow on the connector.

### References

* [Intel, "4-Wire Pulse Width Modulation (PWM) Controlled Fans", Revision 1.3. Sep. 2005.](https://www.glkinst.com/cables/cable_pics/4_Wire_PWM_Spec.pdf)
* [Noctua, "Noctua PWM specifications white paper".](https://noctua.at/pub/media/wysiwyg/Noctua_PWM_specifications_white_paper.pdf)


## Firmware

There are two options for the firmware: one using avr-libc directly and another using the Arduino framework. They do roughly the same thing, but the Arduino version is much shorter.

### Option 1: avr-libc firmware

Connect the programmer to the 6-pin ICSP header on the Arduino:

![ICSP header: 1 MISO, 2 +Vcc, 3 SCK, 4 MOSI, 5 Reset, 6 Gnd](docs/ICSPHeader.jpg)

The connections will vary depending on your programmer.
On my edition of the Bus Pirate, the connections are as follows:

| Arduino ICSP | Bus Pirate    |
| ------------ | ------------- |
| 1 MISO       | MISO (brown)  |
| 2 +Vcc       | +5V (gray)    |
| 3 SCK        | CLK (yellow)  |
| 4 MOSI       | MOSI (orange) |
| 5 RESET      | CS (red)      |
| 6 Gnd        | GND (black)   |

Edit the variables at the top of [firmware_avrlibc/Makefile](firmware_avrlibc/Makefile) to match your microcontroller and programmer.
Build and flash the firmware:

```
cd firmware_avrlibc
make
make flash
```

Afterward, unplug the programmer.

### Option 2: Arduino firmware

1. Set the `PWR SEL` jumper to `USB` on the Arduino.
2. Plug in the USB cable.
3. In the Arduino IDE, open `firmware_arduino/firmware_arduino.ino` and click "Upload".

### References

* [Microchip, "ATmega48/V/88/V/168/V megaAVR® Data Sheet", Rev. 2545B-01/04.](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48_88_168_megaAVR-Data-Sheet-40002074.pdf)


## Usage

1. Set the `PWR SEL` jumper to `EXT` on the Arduino.
2. Apply 12 V power.
3. Connect Arduino to computer with USB cable.
4. Connect with [picocom](https://github.com/npat-efault/picocom) or similar serial terminal software: `picocom /dev/ttyUSB0`
