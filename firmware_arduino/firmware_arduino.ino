#include <util/atomic.h>

// Note that we hardcode use of TIMER1 below, so this can only be pin 9 or 10.
const int pwmPin = 9;

const int potPin = A0;

const int tachPin = 2;

// Number of pulses from the fan's tachometer. The tachometer emits a pulse twice per
// revolution, so the max speed we can support without overflow is 255*60/2 = 7650 RPM.
// The fan is rated for 1700 RPM.
volatile byte pulses = 0;

// How frequently to write RPM to serial port.
unsigned int outputInterval = 1000;
unsigned long lastOutput;
unsigned long nextOutput;

void incrementTachometer() {
  pulses++;
}

void setup() {
  Serial.begin(9600);

  // By default Arduino sets TIMER1 (which includes PWM pin 9) to 8-bit phase correct
  // PWM with a prescaler of /1 and TOP value of 255. This results in a PWM frequency
  // of F_CPU/(2*PRESCALER*TOP) = 16MHz/(2*1*255) = 31kHz, which is beyond the fan's
  // acceptable PWM frequency of 21-28kHz.
  //
  // Change TIMER1's to phase correct PWM with a TOP value of 320, which will result
  // in the recommended PWM frequency of 25kHz.
  TCCR1A = (1 << COM1A1) | (1 << WGM11);
  TCCR1B = (1 << WGM13) | (1 << CS10);
  ICR1 = 320;
  pinMode(pwmPin, OUTPUT);

  // Attach external interrupt to tachometer pin.
  attachInterrupt(digitalPinToInterrupt(tachPin), incrementTachometer, RISING);

  lastOutput = millis();
  nextOutput = lastOutput + outputInterval;
}

void loop() {
  int pot = analogRead(potPin);

  // Scale [0,1023] to [0,320].
  // (   0 + 1) * 5 / 16 =   0
  // (1023 + 1) * 5 / 16 = 320
  analogWrite(pwmPin, (pot + 1) * 5 / 16);

  unsigned long now = millis();
  if (now >= nextOutput) {
    // Fetch and reset pulses atomically so we don't get an off-by-one if the interrupt
    // fires in the middle.
    byte tmpPulses;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
      tmpPulses = pulses;
      pulses = 0;
    }

    // Calculate RPM. There are 2 pulses per revolution, so divide by 2. Times are in
    // milliseconds, so multiply by 60*1000 to get RPM.
    unsigned int rpm = (unsigned long)tmpPulses * 60 * 1000 / (now - lastOutput) / 2;
    Serial.println(rpm);

    lastOutput = now;
    nextOutput += outputInterval;
  }
}
